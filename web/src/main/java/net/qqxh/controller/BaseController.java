package net.qqxh.controller;

import net.qqxh.controller.common.ResponseJsonFactory;
import net.qqxh.persistent.JfUserSimple;
import net.qqxh.persistent.SearchLib;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author jason
 */
public class BaseController{
    @Autowired
    public ResponseJsonFactory responseJsonFactory;
    public JfUserSimple getLoginUser(){
        JfUserSimple jfUserSimple = (JfUserSimple) SecurityUtils.getSubject().getPrincipal();
        return jfUserSimple;
    }
    public SearchLib getSearchLib(){
        JfUserSimple jfUserSimple = (JfUserSimple) SecurityUtils.getSubject().getPrincipal();
        return jfUserSimple==null?null:jfUserSimple.getSearchLib();
    }
}
