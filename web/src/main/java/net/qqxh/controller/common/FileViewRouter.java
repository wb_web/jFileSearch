package net.qqxh.controller.common;

public class FileViewRouter {
    private static String FDF_VIEW = "viewer/pdf";
    private static String TXT_VIEW = "viewer/txt";
    private static String JSON_VIEW = "viewer/json";
    private static String NONE_VIEW= "viewer/fileNotSupported";
    private static String NO_TRANSLATE= "viewer/nontranslate";
    public static String router(String fix) {

        switch (fix) {
            case "pdf":
                return FDF_VIEW;
            case "html":
                return FDF_VIEW;
            case "txt":
                return TXT_VIEW;
            case "sql":
                return TXT_VIEW;
            case "json":
                return JSON_VIEW;
            case "noview":
                return NO_TRANSLATE;
            default:
                return NONE_VIEW;
        }
    }
}
