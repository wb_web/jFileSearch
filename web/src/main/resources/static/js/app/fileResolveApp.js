var singleFileResolveApp = (function () {
    var unique;

    function getInstance() {
        if (unique != undefined) {
            return unique;
        } else {
            unique = new fileResolveApp();
        }
        return unique;
    }

    var fileResolveApp = function (option) {
        this.$url = "fileResolveApp.html";
        this.$window = null;
        this.$windowContent = null;
        this.$isrun = false;

    }

    fileResolveApp.prototype = {
        init: function () {
            if (this.$isrun) {
                this.$window.tag_hover_window();
                return;
            }
            this.$isrun = true;
            this.$window = new Window(1, 450, 566, "系统向导", {closeCallback: this.stop_app.bind(this)});
            this.$window.initialize();
            var _this = this;
            this.$windowContent = this.$window.windowContent;
            $.get(_this.$url, function (r) {
                $(_this.$windowContent).html(r);
                _this.v2m();
            }, "html");
        }
        ,
        stop_app: function () {
            screenLog.destroy();
            if (unique) {
                unique = undefined;
            }
        },
        doAjax: function (url, instance, fn) {
            $.ajax({
                url: url,
                type: 'get',
                data: {},
                cache: false,
                success: function (res) {

                    resResolve(res);
                    instance.stop();
                    if (typeof fn === "function") {
                        fn();
                    }

                },
                error: function (res) {

                    resResolve(res);
                    instance.stop();

                }
            });
        },

        v2m: function () {
            screenLog.init({
                bgColor: '#fff',
                logColor: '#aaa',
                infoColor: 'blue',
                warnColor: 'orange',
                errorColor: 'red',
                freeConsole: false,
                autoScroll: true,
                showEl: this.$windowContent,
                css: 'max-height:500px;'
            });
            var _this = this;

            Ladda.bind('.fileResolveApp button.refreshCache',
                {
                    callback: function (instance) {
                        _this.doAjax('/refreshCache', instance, function () {

                        });
                    }
                });

            Ladda.bind('.fileResolveApp button.checkout',
                {
                    callback: function (instance) {
                        _this.doAjax('/remoteRepository/checkout', instance)
                    }
                });


            Ladda.bind('.fileResolveApp button.formatSystem', {
                callback: function (instance) {
                    var progress = 0;
                    var nowCount = 0;
                    var jsocket = new JWebSocket({
                        uri: 'jfileSocketServer/' + _this.genNonDuplicateID(3),
                        projectName: '',
                        message: function (msg) {
                            debugger;
                            var data = $.parseJSON(msg.data);
                            if (data.totalCount) {
                                var totalCount = data.totalCount;
                                if (nowCount <= data.index) {
                                    nowCount = data.index;
                                }
                                progress = nowCount / totalCount;
                                instance.setProgress(progress);
                            }
                            if (data.finish === true) {
                                jsocket.close();
                                instance.stop();
                            }

                            _this.loggerMsg(data.level,data.msg)
                        },
                        close: function (msg) {
                            instance.stop();
                            _this.loggerMsg("info","关闭连接")
                        }
                    });
                    jsocket.open_connet();
                }
            });
        },
        loggerMsg: function (type, msg) {
            if (type === "error") {
                console.error(msg);
            } else if (type === "info") {
                console.info(msg);
            } else if (type === "warn") {
                console.warn(msg);
            }else {
                console.info(msg);
            }

        }
        , genNonDuplicateID: function (randomLength) {
            return Number(Math.random().toString().substr(3, randomLength) + new Date().getTime()).toString(36)
        }
    }
    return {
        getInstance: getInstance
    }
})();


