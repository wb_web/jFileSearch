package net.qqxh.persistent;

import java.io.File;
import java.io.Serializable;
import java.util.List;

public class FileNode implements Serializable {
    private static final long serialVersionUID = -1L;
    String id;
    String name;
    boolean open;
    boolean isParent;
    int fileCount;
    List<FileNode> sons;
    String parentId;
    boolean  dirEmpty;
    public FileNode() {
    }

    public FileNode(File file ) {
        this.setId(file.getPath());
        this.setParentId(file.getParent());
        if (file.isDirectory()) {
            this.setFileCount(file.list() != null ? file.list().length : 0);
            this.setName(file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf(File.separator) + 1));
            this.setIsParent(true);
        } else {
            this.setName(file.getName());
        }
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public boolean isParent() {
        return isParent;
    }

    public void setParent(boolean parent) {
        isParent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public boolean getIsParent() {
        return isParent;
    }

    public void setIsParent(boolean isParent) {
        this.isParent = isParent;
    }

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public List<FileNode> getSons() {
        return sons;
    }

    public void setSons(List<FileNode> sons) {
        this.sons = sons;
    }


    public boolean getDirEmpty() {
         if(sons==null||sons.size()==0){
             return true;
         }else{
             for(FileNode son:sons){
                  if(!son.isParent){
                      return false;
                  }else{
                     return son.getDirEmpty();
                  }
             }
         }
        return dirEmpty;
    }

    public void setDirEmpty(boolean dirEmpty) {
        this.dirEmpty = dirEmpty;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\":\"" + id +
                "\", \"pId\":\"" + parentId +
                "\", \"name\":\"" + name +
                "\", \"open\":\"" + open +
                "\", \"isParent\":\"" + isParent +
                "\"}";
    }

}

