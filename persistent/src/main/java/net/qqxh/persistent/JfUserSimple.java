package net.qqxh.persistent;

import java.io.Serializable;
import java.util.List;

/**
 * @author jason
 */
public class JfUserSimple implements Serializable {
    private static final long serialVersionUID = -638770777312794913L;
    private String userid;
    private String username;
    private String pwd;
    private List<String> roles;
    private List<String> permissions;
    private String portrait;
    private String searchLibId;
    private SearchLib searchLib;

    private String theme;

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getSearchLibId() {
        return searchLibId;
    }

    public void setSearchLibId(String searchLibId) {
        this.searchLibId = searchLibId;
    }

    public SearchLib getSearchLib() {
        return searchLib;
    }

    public void setSearchLib(SearchLib searchLib) {
        this.searchLib = searchLib;
    }

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getPwd() {
        return pwd;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    public List<String> getPermissions() {
        return permissions;
    }

}